1. Make sure no other program is connected to the nanoKontrol (shut down VoiceMeeter and Macro buttons)
2. Load `NanoKontrolSettings.nkrl2_data` onto your board with [Kortg Kontrol Editor](https://www.korg.com/us/support/download/software/0/159/1354/)
3. Open VoiceMeeter and import `voicemeeter_slider_nanokontrol2.xml`
4. Open Macro Buttons (installed together with VoiceMeeter) and import `macro_buttons_nanokontrol2_buttonmap.xml`

This is an example configuration with the following settings:

- Slider 1 to 3 control your hardware inputs (strip). It's corresponding Mute and Solo buttons work as expected with corresponding LED control. The Record button toggles B2
- Sliders 4 to 5 work like the first 3, but then for the virtual inputs.
- Sliders 6 to 8 control your 3 hardware outputs (bus). The mute buttons work as expected, but the solo and record buttons are not set in Macro Buttons.
- The previous/next/stop/play buttons work as Media Buttons like you would find on your keyboard.
- The led for the play button turns on if there is input detected on strip 5 (Called "Music")
- The led for the record button turns on if there is input detected on strip 4 (Called "Voice")
